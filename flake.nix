{
  description = "Zaravi's Home Manager Environment";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixgl.url = "github:nix-community/nixGL";
    nix-flatpak.url = "github:gmodena/nix-flatpak";
    stylix.url = "github:danth/stylix";

    nix-darwin = {
      url = "github:LnL7/nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-on-droid = {
      url = "github:nix-community/nix-on-droid/master";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };
  };

  outputs =
    inputs @ { self
    , nixpkgs
    , home-manager
    , nixgl
    , stylix
    , nix-flatpak
    , nix-darwin
    , nix-on-droid
    , ...
    }:

    let
      user = "zaravi";
    in

    {
      homeConfigurations = (
        let
          pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
            overlays = [ nixgl.overlay ];
          };
          system = "x86_64-linux";
        in
        {
          bazzite = import ./hosts/home/bazzite {
            inherit
              inputs pkgs
              user system
              home-manager
              stylix
              nix-flatpak
              ;
          };
        }
      );

      nixosConfigurations = (
        let
          pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
          };
          lib = nixpkgs.lib;
          system = "x86_64-linux";
        in
        {
          virtual = import ./hosts/nixos/virtual {
            inherit
              inputs pkgs lib
              user system
              home-manager
              stylix
              nix-flatpak
              ;
          };
        }
      );

      darwinConfigurations = (
        let
          pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
          };
          system = "aarch64-darwin";
        in
        {
          quartzbox = import ./hosts/darwin/quartzbox {
            inherit
              inputs pkgs
              user system
              home-manager
              nix-darwin
              ;
          };
        }
      );

      nixOnDroidConfigurations = (
        let
          pkgs = import nixpkgs {
            system = "aarch64-linux";
            config.allowUnfree = true;
            overlays = [ nix-on-droid.overlays.default ];
          };
          lib = nix-on-droid.lib;
        in
        {
          obscureresurrection = import ./hosts/nix-on-droid/obscureresurrection {
            inherit
              inputs
              pkgs
              lib
              home-manager
              nix-on-droid
              ;
          };
        }
      );
    };
}
