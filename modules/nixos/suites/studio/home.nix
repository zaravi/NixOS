{ pkgs, ... }:

{
  home = {
    packages = with pkgs;
      [
        helvum # "Pipewire patchbay."
        pavucontrol # "Volume and source control."
        easyeffects # "Pipewire effect sink."
        shortwave # "Track hunting via radio"
        mousai # "Song sniffer."
        parabolic # "Url downloader."
        fretboard # "Guitar chords"
        blanket # "Cozy noise."
        amberol # "Simple music player."
        rhythmbox # "Music player"
        eartag # "Simple metadata editor."
        cavalier # "Simple visualizer."
        chromatic # "Guitar tuner."
      ];
  };
}
