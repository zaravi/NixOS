{ ... }:

{
  services = {
    pipewire = {
      enable = true;
      audio.enable = true;
      alsa.enable = true;
      pulse.enable = true;
      jack.enable = true;
      wireplumber.enable = true;
      socketActivation = true;
    };
    flatpak = {
      update.onActivation = true;
      packages = [
        "com.bitwig.BitwigStudio"
        "org.freac.freac"
      ];
    };
  };
}
