{ pkgs, inputs, user, impermanence, nix-flatpak, ... }:

{
  networking = { 
    firewall = {
      allowedTCPPorts = [ 8384 27036 27037 62169 ];
      allowedUDPPorts = [ 27031 27036 62169 ];
    };
  };

  programs = {
    steam = {
      enable = true;
      remotePlay.openFirewall = true;
      extraPackages = with pkgs; [
        gamescope
        steamcontroller
      ];
    };
    gamemode.enable = true;
  };
  hardware.steam-hardware.enable = true;

  services.flatpak = {
    update.onActivation = true;
    packages = [
      "com.valvesoftware.SteamLink"
    ];
  };
}
