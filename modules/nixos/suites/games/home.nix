{ lib, pkgs, inputs, user, ... }:

{
  home = {
    packages = with pkgs;
      [
        prismlauncher
        lutris
        dolphin-emu
        # dolphin-emu-primehack
        space-cadet-pinball
        retroarch
        gamemode
        cemu
        protonup
        vkbasalt-cli
        # inputs.nix-citizen.packages.${system}.star-citizen
      ];
  };
}
