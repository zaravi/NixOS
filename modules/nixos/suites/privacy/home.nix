{ pkgs, ... }:

{
  programs = {
    home-manager.enable = true;
  };

  home = {
    packages = with pkgs;
    [
      mullvad-browser
      tor-browser
    ];
  };
}
