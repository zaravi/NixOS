{ pkgs,... }:

{
  programs = {
    home-manager.enable = true;
  };

  home = {
    packages = with pkgs;
    [
      # Cracking
      aircrack-ng
      john
      hashcat
      hashcat-utils
      thc-hydra

      # Recon
      nmap
      traceroute

      # Exploitation
      msfpc
      burpsuite
    ];
  };
}
