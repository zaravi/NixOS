{ pkgs, user, ... }:

let
  dog = pkgs.writers.writeNu "dog" /*nushell*/ ''
    let ascii = $"
           ..                                                                                              
           Od:        .,;'.,                            
          :Oooo;o,,, :x,   .c                                                                              
        ccdkxkkkOxkx:K0. .  ,.                                                                             
        .ox00KkO0OkkcKK: ,, .'                                                                             
       .cxKKX0kKl,,,:0Ko.c,;c                                                                              
        :kx0XKXXKk:,,K0c;d0kc...                                                                           
       :lx:lxKdxOKKxOKKOoXNkkOo:.;::;.          ........                                                   
      x0K0dokO;ocKKO::xl:o0dOk0dOXX0OO:::clodxOkkOOOOOOOOOxold'..                                          
      0NNXKKKKKKKKKd,,;,:x:,,,x00O0K0KK000000000kkkkkxxxxkdco0KKKd.                                        
      .kKKKKKKK0xl;;:loolo:,;:dkkO0Ooolll:cc::oOOddddddkO0KOOKcdk0c'                                       
        ',,;;::;dOOk0O00cc;,xoOxddOOocc:cc:c:clx0ddxxodKKKO:cKdl..oKOo;.      ......:                      
               ;OXXXXXXKxoO0000OkxkOo;olcccxooxkKOxc:dddccxoxKlo  :0KKKK0kxxdo;  .':.                      
                colOXXKKK0Okxdc:coO000KKK00OOO0KXK0kO:d.  .:lOkl..0XKKKKKKKK0o.  ..,..                     
                 .cdKXXKKlc,.   .:oxkOOOOxxxdxddddllkol,    olKxddKXXKKKKKKK0ol,....                     
                  .c0XXKx:o.      d:KK00d....c::x:clokxl:'.,loKoxk0dXX0XXXKKXXo'..                         
                   .okKOolkcc. ..lcck000o';ll,. dl,,,,ddodxxdoc;..c;'x .';:loo.                            
                     dOXo:oxcccc:,x0XKKKc:;.    dOodxlxo;:,,'.;c,;do.                                      
                      kXOoxkcolccdKK,,..        .;klxKOdOl:;;..lc0Kko'                                     
                      ,KXXXKl,;OxcdX.                 ,dKXKXKdldOKKKKK;                                    
                      .x0XXXk; c;,lKO.                  .oXXXl .,ckKKxl.                                   
                       xxXXO.l.,kd0Ok                    cXX0     ckc;:                                    
                       dxXX: cOKOlc.                    '0XX,     :,,'.                                    
                       oXXX. dKK:,;                 .loxXXXo     .:.;o  ...............                    
                       xXXk  kKK,:.            .....d0KK0Oo...;:;,.;:c...........                       
                    ..:XXO'  0K0,;.......,,,,,,,,,,,,,,,,,,,,,Ollo:;:......                                
                  ;OKO0Xd..,x0xoo............................,,,.                                        
             .....;colc;kOkododo...................                                                       
            ..............oldl:,'......                                                                      
    "
    let root_ascii = $"
                 .                                                                                                                                                  
                .Kx.                                                                                                                                                
                l0ok;.           .':cc;,.;;                                                                                                                         
               .KOkd:d;.o:. ;.  ,cd;.     :;                                                                                                                        
           ...;kKxoddkddoxxoxc :0Od.      .o                                                                                                                        
           :xlddxxkdxkOOOOdOOxcOKKd'  .    ;:                                                                                                                       
            .cxkkO0000O0KKK0K0cOKKk: .:..  .o                                                                                                                       
            ,ldxO00KXdkkOkdoxc;0KKX; .'d,  ,c                                                                                                                       
          .xldO00KKKkdO0d,,,,,,xKKKO;.l:...o.                                                                                                                       
          .,,cKXNXXXXXXXOdl;,,,dKKk'.,cld0d,                                                                                                                        
          .ccOlc;XKKKKXKKKKO:,;xKKOOOlONNOl:,:;l                                                                                                                    
          l:lKc(ansi red):.(ansi reset)OoKXxookKKKKOKKKKKxlxNWNOokOklo .;cccc:.                                                                                                           
         lOoOKkoc,lXo(ansi red).c(ansi reset)l.xKKK0old0Kk:cdOKK0XKK0l,KKKKOOO0l  ..',;;:ccldoooodddddddddooloc:,..c.                                                                     
        :NNNNNOdxOKK0dllo0KKKk,,,c:,;clxk::dooXkcKKKXX0OO00xxkxxxxkkkkO0OOOOOOO000000KKXKKKK0Xdlc:,'.                                                               
        'WNNNNXKKKKKKKKKKKKKKd,,,,,,,lOl:,,,,:k00OkO0KK000KOO00KKKKKK000K0OOkkkkkkxxxxxxK::ookKKKKKKd                                                               
         oNXKKKKKKKKKKKKKK0xc,,,cl:;,,ol,,,,,:o0XKKKK0Okk0kxxdxdxcocccxOO0dddddddddddddxOOlllkKkO0KK;                                                               
          :OKKKKKKKKKKKkoc;,;:coolkxddok;,,:;odxxddOOOo:::cl:lc:ccc;lc,xO0kddddddddxO0KXKKK00KK;,dld0Ol;.                        .                                  
            ',,;;:::ccc;;OOOOkK0kxNOoccc,,;x,OOddddx0OO,;,o;:lcdll;cd:clOOKdddddkkdkKKKKKx:;;0KO,o' .0KKKkl;.            .......,l;                                 
                      .d0XXXXXXXXXXKo,,;odxK0O00kddd0OOo;lo'd.O,:,,;,.d'xOKxdkkdc,cKKkxdkkl,;OKd,d.  .dKKKKKKkoc;,,,;;;;...   .;;o                                  
                       .:XXXXXXXXXNK0KOxXXOOO00kOOxdOOOd;,k,k:olxx0Oxxxk00KK0l,,ckxll:;:oodkKKKo,x   oXKKKKKKKKKKKKKKxxd;  ..;,,l                                   
                        cX0OkXNXXXKNXX0O0K0kdoccc;ok0000000000KKKKKK0000KXXKK0kx0lcx'    ,lllOK0,x. .WXKKKKKKKKKKKKKKKO:.  ....c;.                                  
                         .   xKKKKKKKKkdlol::;;,;oo:kKOOO00000OkkxxxdxOKXKKKOdxK0,x:       :c;0Kd:o,lXXXKKKKKKKKKKKKKO;.       ..:c.                                
                          .ox0KNNNXKKOo,l,        ,dcokxkOOOO0xdxxkxdkxcdddo:cloOk;x:      .k'xKKxlodXNXXKKKKKKKKKKKKKdoc;;,,:;,'.                                  
                           .:xOXXXKKKxcxl           k;lXK0000Xc;,,'..d,,;ldccdddxKkcll'    'O,xKKxk0KK0XXXXXXXXKKKKKXXXKd:,'..                                      
                             oXXKNKK0l,;dl;        ;d;xKXOOOOX    .:lll:;cX:,,,,;;kxxclc;;colxKOl:,:oc:'OXXcoxOKNXXXKKXo                                            
                              coxONk:doOk,lo.   ..cx,k.0KOOO00,,:ldoc;.  .Nx,,,,,,c0ddkkxxkOkdc;o,  :;k. cO    ..,;:cll:                                            
                               l0lXXd:;ckk;;dlllooc,oXdNKKKKXocodc;.      Kxc,;cc,;Odd,lodlc;;;:l;',::xk.                                                           
                                lkXXkdllooOo,,,,,,,:0NKXKXX0x:;,.        cXXXo0XXkkKll,l..'.   'l:c:ckxc:                                                           
                                 xXXXdlldOdokl;;;;cOKKx;,'..             ..'KKok0XKdoOk:l:,,c.  'l;kKK0d:d.                                                         
                                  0XXX00Xkx ;cOKK0kx0Kd                     ..  .,odkKX0KOkxxxl;c:dKKKKK0Kx.                                                        
                                  oOXXXXXXNo;;.dK;,,oKKc,                           'lkXXXXXXXc;:x000KKKKKKoc                                                       
                                  coxXXXXXXd;. 'k,,,oKKO,                              ,XXXXXK.   .'cxKKKKo'd                                                       
                                  ,xlKXXXNl:;; .xl;c0KKX.                              .NXXXX:       dKKkl;l;                                                       
                                  .0lOXXXK  :x':OKKKOKcd'                              lKXXX0        ,kc,;,d                                                        
                                   Kl0XXXd  clKKKOoc;l                                .OXXXX.        d;,:'.:                                                        
                                   0kXXXN,  ldKKK:,,l                               .;0XXXXo        .d;,.;d                 ..........                              
                                   xXXXXX   l0KKO,,;'                           :kOKXXXXXX0         l;. ccl ............;;;;;;;;;;...                              
                                   kXXXXx   lKKKd,,c                        ....KKKKKKKXOd.....',;,,. 'c;oc;;;;;;;;;;;;;;;;;......                                  
                                  ;NXXXX;   xKKKo,l.      .............;;;;;;;;cllllc;;;;;;;;kocc:,;;,cd;;;;;;;;;;.........                                          
                              ..'lkXXXO;   .0KKKl;o...........................................:0xllollc;:.....                                                      
                            lO0kxOOXXd'....xKKOo,d'............................................,::;;,'.                                                             
                        ...;0KKKKKKOc;:cc;okxolxdc.........................................                                                                        
                    .........:llc:,oNKxdcooddxo................................                                                                                        
                  ..................O0x00xxdol:.............                                                                                                       
                   ..................',;;,'...                                                                                                                      
    "
    if (is-admin) {
      echo $root_ascii
    } else {
      echo $ascii
    }
  ''; # IMPORTANT: This.
in

{
  programs = {
    home-manager.enable = true;
    git = {
      userName = "${user}";
      userEmail = "${user}";
    };
    # obs-studio = {
    #   enable = true;
    #   plugins = with pkgs.obs-studio-plugins; [
    #     wlrobs
    #     obs-backgroundremoval
    #     obs-pipewire-audio-capture
    #     waveform
    #     obs-tuna
    #     obs-vkcapture
    #     obs-gstreamer
    #     obs-source-record
    #     obs-composite-blur
    #   ];
    # };
  };

  home = {
    file = {
      ".local/nu/dog".source = dog;
    };
    stateVersion = "24.05";
    username = "${user}";
    packages = with pkgs;
      [
        # # USUAL SUSPECTS
        nix-index
        # coreutils-full
        # pciutils
        # usbutils
        # diffutils
        # findutils
        # utillinux
        # gnugrep
        # gnupg
        # gnused
        # gawk

        # # COMPRESSION TOOLS
        # gnutar
        # bzip2
        # gzip
        # xz
        # zip
        # unzip
        # p7zip

        lazygit
        # btop
        # broot
        fzf

        # # WEB
        # brave
        # lynx
        # wget
        # curl

        # # VIDEO
        # ffmpeg # "Multimedia everything."
        # gst_all_1.gstreamer # "Gstreamer codecs."
        # celluloid # "Video player"

        # kdenlive # "Video Editing."
        # handbrake # "Video Compression."
        # libsForQt5.qt5.qtwayland

        # # IMAGES
        loupe # "Image viewer."
        # inkscape # "Vector Graphics."
        # gimp # "Image editing."
        ueberzugpp # "Renderer"

        # # 3D GRAPHICS
        # # blender-hip # "Blender for AMDGPU"

        # # Network Essentials
        # wireguard-tools
        # bluez

        # # Sync
        # nextcloud-client

        # # Chat-Clients
        # signal-desktop
        # telegram-desktop
        # fractal # "Matrix."
        # vesktop # "Discord."

        # appimage-run # "Wrapper to allow AppImages to work."

        # scrcpy # "Remote Android."
        # remmina # "Remote Desktop."

        # # "FILE MANAGEMENT"
        # nautilus
        # nautilus-python
        # nautilus-open-any-terminal
        # czkawka
        # szyszka
        # meld
        # raider

        # baobab # "Disk Usage Visual."
        # gnome-obfuscate # "Data cleaning."
        # metadata-cleaner # "Data cleaning."
        # newsflash # "News RSS feed."
        # gnome-feeds # "Youtube RSS feed."
        # warp # "Magic wormhole."
        # popsicle # "USB Flasher"

        # textpieces # "Useful toolkit."
        # pika-backup # "Backups"
        ghostty

        # "Terminal goods."
        cbonsai
        cmatrix
        tty-clock
        tldr

        picard # "Musicbrainz metadata suite."
        # ente-auth # "Authenticator."
      ];
  };
}
