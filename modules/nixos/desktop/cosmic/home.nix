{ lib, ... }:

{
  home = {
    file = {
      "cosmic" = {
        source = ./cosmic;
        target = ".config/cosmic";
        recursive = true;
        enable = true;
      };
    };
  };
}
