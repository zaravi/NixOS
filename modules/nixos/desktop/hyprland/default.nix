{ pkgs, user, inputs, ... }:

{
  nix = {
    settings = {
      substituters = [ "https://hyprland.cachix.org" ];
      trusted-public-keys = [ "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc=" ];
      auto-optimise-store = true;
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };

    package = pkgs.nixFlakes;
    registry.nixpkgs.flake = inputs.nixpkgs;
    extraOptions = ''
      experimental-features = nix-command flakes
      keep-outputs          = true
      keep-derivations      = true
    '';
  };

  nixpkgs.config = {
    allowUnfree = true;
  };

  stylix = {
    targets.chromium.enable = false;
    enable = true;
    homeManagerIntegration.followSystem = true;

    image = ./stylix_needs_a_bg.png;
    polarity = "dark";

    base16Scheme = {
      base00 = "262626"; # #262626 Default Background
      base01 = "262626"; # #262626 Lighter Background (Used for status bars, line number and folding marks)
      base02 = "4a4949"; # #262626 Selection Background
      base03 = "666666"; # #333333 Comments, Invisibles, Line Highlighting
      base04 = "999999"; # #333333 Dark Foreground (Used for status bars)
      base05 = "fffafa"; # #fffafa Default Foreground, Caret, Delimiters, Operators
      base06 = "fffafa"; # #fffafa Light Foreground (Not often used)
      base07 = "fffafa"; # #fffafa Light Background (Not often used)

      base08 = "d4ac77"; # #d4ac77 Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
      base09 = "44bfeb"; # #44bfeb Integers, Boolean, Constants, XML Attributes, Markup Link Url
      base0A = "fffafa"; # #fffafa Classes, Markup Bold, Search Text Background
      base0B = "d4ac77"; # #f51252 Strings, Inherited Class, Markup Code, Diff Inserted
      base0C = "fffafa"; # #d4ac77 Support, Regular Expressions, Escape Characters, Markup Quotes
      base0D = "9c63fe"; # #44bfeb Functions, Methods, Attribute IDs, Headings
      base0E = "d4ac77"; # #9c63fe Keywords, Storage, Selector, Markup Italic, Diff Changed
      base0F = "00ff00"; # #00ff00 Deprecated, Opening/Closing Embedded Language Tags, e.g. <?p
    };

    fonts = {
      monospace = {
        package = pkgs.nerdfonts.override { fonts = [ "Mononoki" ]; };
        name = "Mononoki Nerd Font Mono";
      };

      sansSerif = {
        package = pkgs.nerdfonts.override { fonts = [ "Mononoki" ]; };
        name = "Mononoki Nerd Font";
      };

      serif = {
        package = pkgs.nerdfonts.override { fonts = [ "Mononoki" ]; };
        name = "Mononoki Nerd Font";
      };

      sizes = {
        applications = 12;
        terminal = 14;
        desktop = 10;
        popups = 10;
      };
    };

    opacity = {
      applications = 1.0;
      terminal = 0.9;
      desktop = 1.0;
      popups = 1.0;
    };

    cursor = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Ice";
      size = 24;
    };

  };

  systemd = {
    user.services.polkit-gnome-authentication-agent-1 = {
      description = "polkit-gnome-authentication-agent-1";
      wantedBy = [ "graphical-session.target" ];
      wants = [ "graphical-session.target" ];
      after = [ "graphical-session.target" ];
      serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
        Restart = "on-failure";
        RestartSec = 1;
        TimeoutStopSec = 10;
      };
    };
  };

  environment = {
    variables = {
      XDG_DATA_HOME = "/home/${user}/.local/share";
      XDG_DOWNLOAD_DIR = "/home/${user}/Downloads";
    };

    etc."os-release".text = ''
      PRETTY_NAME="ZaraviOS"
      NAME="ZaraviOS"
      ID=zaravios
      HOME_URL="https://gitlab.com/zaravi/os"
      SUPPORT_URL="https://gitlab.com/zaravi/os"
      BUG_REPORT_URL="https://gitlab.com/zaravi/os"
    '';
  };
  system.nixos.label = "ZaraviOS";

  xdg.portal = {
    enable = true;
    wlr.enable = true;
    xdgOpenUsePortal = true;
    extraPortals = [
      # pkgs.xdg-desktop-portal-hyprland
      pkgs.xdg-desktop-portal-gtk
    ];
    # config.common.default = [ "hyprland" ];
    configPackages = [
      pkgs.gnome-session
    ];
  };

  fonts.packages = with pkgs; [
    (nerdfonts.override { fonts = [ "Mononoki" ]; })
  ];

  systemd = {
    services.polkit-gnome-authentication-agent-1 = {
      description = "polkit-gnome-authentication-agent-1";
      wantedBy = [ "graphical-session.target" ];
      wants = [ "graphical-session.target" ];
      after = [ "graphical-session.target" ];
      serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
        Restart = "on-failure";
        RestartSec = 1;
        TimeoutStopSec = 10;
      };
    };
  };

  programs = {
    hyprland = {
      enable = true;
      package = inputs.hyprland.packages.${pkgs.stdenv.hostPlatform.system}.hyprland;
      portalPackage = inputs.hyprland.packages.${pkgs.stdenv.hostPlatform.system}.xdg-desktop-portal-hyprland;
    };
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
    mtr.enable = true;
    dconf.enable = true;
    gnome-disks.enable = true;
  };

  services = {
    pipewire = {
      enable = true;
      audio.enable = true;
      alsa.enable = true;
      pulse.enable = true;
      jack.enable = true;
      wireplumber.enable = true;
      socketActivation = true;
    };

    gnome = {
      tracker.enable = true;
      tracker-miners.enable = true;
      sushi.enable = true;
      glib-networking.enable = true;
      gnome-keyring.enable = true;
    };

    gvfs.enable = true;
    blueman.enable = true;

    flatpak = {
      enable = true;
      update.onActivation = true;
      packages = [
        "de.schmidhuberj.DieBahn"
        "com.github.liferooter.textpieces"
        "org.gaphor.Gaphor"
        "it.mijorus.collector"
        "com.github.tchx84.Flatseal"
      ];
    };
  };

  security.polkit = {
    enable = true;
    extraConfig = ''
      polkit.addRule(function(action, subject) {
        if (
          subject.isInGroup("users")
            && (
              action.id == "org.freedesktop.login1.reboot" ||
              action.id == "org.freedesktop.login1.reboot-multiple-sessions" ||
              action.id == "org.freedesktop.login1.power-off" ||
              action.id == "org.freedesktop.login1.power-off-multiple-sessions" ||
            )
          )
        { return polkit.Result.YES; }
      })
    '';
  };
}
