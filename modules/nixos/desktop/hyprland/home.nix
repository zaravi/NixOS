{ inputs, pkgs, config, hy3, ... }:

#      ~ Welcome to Zaravi's ~ 
#     dBP dBP dBP dBP dBBBBBb dBBBBBb    dBP dBBBBBb     dBBBBb  dBBBBb
#                dBP      dB'     dBP             BB        dBP     dBP
#   dBBBBBP     dBP   dBBBP'  dBBBBK   dBP    dBP BB   dBP dBP dBP dBP
#  dBP dBP     dBP   dBP     dBP  BB  dBP    dBP  BB  dBP dBP dBP dBP
# dBP dBP     dBP   dBP     dBP  dB' dBBBBP dBBBBBBB dBP dBP dBBBBBP home.nix
#
# "Where barking at the problem worked."

let
  mod = "SUPER";
  alt = "ALT";
  shift = "SHIFT";
  control = "CONTROL";

  l = "m";
  d = "n";
  u = "e";
  r = "i";

  terminal = "alacritty";

  startupScript = pkgs.pkgs.writeShellScriptBin "start" /*sh*/ ''
    ${pkgs.waybar.overrideAttrs (oldAttrs: { mesonFlags = oldAttrs.mesonFlags ++ [ "-Dexperimental=true "]; })}/bin/waybar &
    dbus-update-activation-environment --systemd --all
    # ${pkgs.light} -S 1
    # ${pkgs.wlsunset} -t 1000 -T 6500 -S 06:00 -s 18:00  -d 1800 -g 1
    swww-daemon
  '';

  random-wallpaper = pkgs.writers.writeNu "random-wallpaper" /*nushell*/ ''
    let wallpaper_dir = '/home/zaravi/Pictures/artwork'
    let image_files = (glob $"($wallpaper_dir)/**/*" | where {|it| $it =~ '\.(jpg|jpeg|png|gif)$'})
    let random_image = ($image_files | shuffle | first)
    ${pkgs.swww}/bin/swww img $random_image --fill-color 262626 --transition-type grow --transition-step 1 --transition-duration 1 --transition-fps 144 --resize fit --filter Lanczos3
  '';

  next-wallpaper = pkgs.writers.writeNu "next-wallpaper" /*nushell*/ ''
    let wallpaper_dir = '/home/zaravi/Pictures/artwork'
    let current_wallpaper = (^swww query | lines | first | split row ':' | last | str trim)
    let all_wallpapers = (glob $"($wallpaper_dir)/**/*" | where {|it| $it =~ '\.(jpg|jpeg|png|gif)$'})
    let next_wallpaper = if ($all_wallpapers | any {|w| $w == $current_wallpaper }) {
      $all_wallpapers 
      | skip until {|w| $w == $current_wallpaper } 
      | skip 1 
      | first
    } else {
        $all_wallpapers | first
    }
    let next_wallpaper = if $next_wallpaper == null { 
        $all_wallpapers | first
    } else { 
        $next_wallpaper
    }
    ^swww img $next_wallpaper --fill-color 262626 --transition-type grow --transition-step 1 --transition-duration 1 --transition-fps 144 --resize fit --filter Lanczos3
  '';

  previous-wallpaper = pkgs.writers.writeNu "previous-wallpaper" /*nushell*/ ''
    let wallpaper_dir = '/home/zaravi/Pictures/artwork'
    let current_wallpaper = (^swww query | lines | first | split row ':' | last | str trim)
    let all_wallpapers = (glob $"($wallpaper_dir)/**/*" | where {|it| $it =~ '\.(jpg|jpeg|png|gif)$'})
    let previous_wallpaper = if ($all_wallpapers | any {|w| $w == $current_wallpaper }) {
        $all_wallpapers 
        | reverse
        | skip until {|w| $w == $current_wallpaper } 
        | skip 1 
        | first
    } else {
        $all_wallpapers | last
    }
    let previous_wallpaper = if $previous_wallpaper == null { 
        $all_wallpapers | last
    } else { 
        $previous_wallpaper
    }
    ^swww img $previous_wallpaper --fill-color 262626 --transition-type grow --transition-step 1 --transition-duration 1 --transition-fps 144 --resize fit --filter Lanczos3
  '';
in

{
  imports = [ inputs.ags.homeManagerModules.default ];
  home = {
    file = {
      "waybar" = {
        source = ./dotfiles/waybar;
        target = ".config/waybar";
        enable = true;
      };

      ".local/nu/random-wallpaper".source = random-wallpaper;
      ".local/nu/next-wallpaper".source = next-wallpaper;
      ".local/nu/previous-wallpaper".source = previous-wallpaper;

      ".config/qt5ct/qt5ct.conf".text = ''
        [Appearance]
        style=kvantum
      '';

      ".config/qt6ct/qt6ct.conf".text = ''
        [Appearance]
        style=kvantum
      '';
      ".config/Kvantum/kvantum.kvconfig".text = ''
        [General]
        theme=Stylix
      '';
    };

    packages = with pkgs; [
      hyprland-protocols
      wl-clipboard
      wtype
      wofi
      hyprshot
      hyprpicker
      swww
      libsForQt5.qt5ct
      libsForQt5.qtstyleplugin-kvantum
      qt6Packages.qt6ct
      qt6Packages.qtstyleplugin-kvantum
      inputs.ags.packages.${pkgs.system}.io
      inputs.ags.packages.${pkgs.system}.notifd
    ];
  };

  stylix = {
    targets = {
      hyprland.enable = false;
      hyprpaper.enable = false;
    };
  };

  services.hypridle = {
    enable = false;
    #   settings = {
    #     general = {
    #       ignore_dbus_inhibit = false;
    #       lock_cmd = "pidof hyprlock || hyprlock";
    #       before_sleep_cmd = "loginctl lock-session";
    #       after_sleep_cmd = "hyprctl dispatch dpms on";
    #     };
    #     listener = [
    #       {
    #         timeout = 500;
    #         on-timeout = "loginctl lock-session";
    #       }
    #       {
    #         timeout = 500;
    #         on-timeout = "hyprctl dispatch dpms off";
    #         on-resume = "hyprctl dispatch dpms on";
    #       }
    #     ];
    #   };
  };

  programs = {
    home-manager.enable = true;
    hyprlock = {
      enable = true;
      settings = {
        general = {
          disable_loading_bar = true;
          grace = 5;
          hide_cursor = true;
          no_fade_in = false;
          ignore_empty_input = true;
        };
        background = [
          {
            path = "screenshot";
            blur_passes = 4;
            blur_size = 32;
          }
        ];
        input-field = [
          {
            size = "230, 40";
            position = "0, -50";
            monitor = "";
            dots_center = true;
            fade_on_empty = true;
            fade_timeout = "250";
            rounding = "9";
            font_color = "rgba(fffafaff)";
            inner_color = "rgba(262626ff)";
            outer_color = "rgba(d4ac77ff)";
            outline_thickness = 3;
            placeholder_text = "";
            fail_text = "";
            fail_transition = "100";
            fail_color = "rgb(${config.stylix.base16Scheme.base08})";
            shadow_passes = "1";
          }
        ];
        label = [
          {
            monitor = "";
            text = "- Welcome Back -";
            text_align = "center";
            color = "rgba(d4ac77ff)";
            font_size = "18";
            font_family = "${config.stylix.fonts.monospace.name}";

            position = "0, 125";
            halign = "center";
            valign = "center";
          }
          {
            monitor = "";
            text = "$TIME";
            text_align = "center";
            color = "rgba(fffafaff)";
            font_size = "64";
            font_family = "${config.stylix.fonts.monospace.name}";

            position = "0, 50";
            halign = "center";
            valign = "center";
          }
        ];
      };
    };
    ags = {
      enable = true;
      # configDir = ./ags;
      extraPackages = with pkgs; [
        gtksourceview
        webkitgtk
        accountsservice
      ];
    };
  };

  gtk = {
    enable = true;
    iconTheme = {
      package = pkgs.reversal-icon-theme.override {
        colorVariants = [ "-black" ];
      };
      name = "Reversal-black-dark";
    };
  };

  xdg.mimeApps.defaultApplications = {
    "image/*" = [ "loupe.desktop" ];
    "video/*" = [ "celluloid.desktop" ];
  };

  wayland.windowManager.hyprland = {
    enable = true;
    # plugins = [ hy3.packages.x86_64-linux.hy3 ];
    xwayland.enable = true;
    systemd = {
      enable = true;
      variables = [ "--all" ];
    };
    settings =
      {
        "$bark" = "";

        exec-once = ''${startupScript}/bin/start'';

        env = [
          "XDG_SESSION_TYPE            = wayland"
          "XDG_SESSION_DESKTOP         = Hyprland"
          "XDG_CURRENT_DESKTOP         = Hyprland"
          "XDG_DOWNLOAD_DIR            = /home/zaravi/Downloads/"

          "WLR_NO_HARDWARE_CURSORS     = 1 Hyprland"
          "HYPRCURSOR_THEME            = Bibata-Modern-Ice"
          "HYPRCURSOR_SIZE             = 12"
          "XCURSOR_THEME               = Bibata-Modern-Ice"
          "XCURSOR_SIZE                = 12"
          "GTK_THEME                   = adw-gtk3-dark"
          "GDK_SCALE                   = 0.1"

          "_JAVA_AWT_WM_NONREPARENTING = 1"
          "MOZ_ENABLE_WAYLAND          = 1"
          "SDL_VIDEODRIVER             = wayland"

          "QT_STYLE_OVERRIDE           = adwaita-dark"
          "QT_QPA_PLATFORMTHEME        = qt5ct"
          "QT_STYLE_OVERRIDE           = kvantum"
        ];

        bind = [
          "${control} ${shift} ${alt}, r, exec, ~/.local/nu/random-wallpaper"
          "${control} ${shift} ${alt}, i, exec, ~/.local/nu/next-wallpaper"
          "${control} ${shift} ${alt}, m, exec, ~/.local/nu/previous-wallpaper"

          "${mod}, return, exec, ${terminal}"
          "${mod} ${shift}, c, killactive,"
          "${mod} ${shift}, q, exit,"
          "${mod}, t, togglefloating,"
          "${mod} ${shift}, return, exec, wofi --show drun"
          "${mod}, P, pseudo,"
          "${mod}, J, togglesplit,"
          "${mod}, R, exec, kitty -e ranger"
          "${mod}, B, exec, bookmark"

          "${mod}, ${l}, movefocus, l"
          "${mod}, ${d}, movefocus, d"
          "${mod}, ${u}, movefocus, u"
          "${mod}, ${r}, movefocus, r"

          "${mod} ${shift}, ${l}, movewindow, l"
          "${mod} ${shift}, ${d}, movewindow, d"
          "${mod} ${shift}, ${u}, movewindow, u"
          "${mod} ${shift}, ${r}, movewindow, r"

          "${mod}, 1, workspace, 1"
          "${mod}, 2, workspace, 2"
          "${mod}, 3, workspace, 3"
          "${mod}, 4, workspace, 4"
          "${mod}, 5, workspace, 5"
          "${mod}, 6, workspace, 6"
          "${mod}, 7, workspace, 7"
          "${mod}, 8, workspace, 8"
          "${mod}, 9, workspace, 9"
          "${mod}, 0, workspace, 10"

          "${mod} ${shift}, 1, movetoworkspace, 1"
          "${mod} ${shift}, 2, movetoworkspace, 2"
          "${mod} ${shift}, 3, movetoworkspace, 3"
          "${mod} ${shift}, 4, movetoworkspace, 4"
          "${mod} ${shift}, 5, movetoworkspace, 5"
          "${mod} ${shift}, 6, movetoworkspace, 6"
          "${mod} ${shift}, 7, movetoworkspace, 7"
          "${mod} ${shift}, 8, movetoworkspace, 8"
          "${mod} ${shift}, 9, movetoworkspace, 9"
          "${mod} ${shift}, 0, movetoworkspace, 10"

          "${control} ${mod}, ${l}, workspace, e-1"
          "${control} ${mod}, ${r}, workspace, e+1"

          "${control} ${mod} ${shift}, ${l}, movetoworkspace, e-1"
          "${control} ${mod} ${shift}, ${r}, movetoworkspace, e+1"

          "${mod}, l, exec, pidof hyprlock || hyprlock"
          "$bark, F10, fullscreen, 1"
          "$bark, F11, fullscreen"
          "$bark, print, exec, hyprshot -m output -c -o ~/Pictures/Screenshots/hyprshot"
          "${shift}, print, exec, hyprshot -m region -o ~/Pictures/Screenshots/hyprshot"

          "${mod}, mouse_down, workspace, e+1"
          "${mod}, mouse_up, workspace, e-1"
        ];

        bindm = [
          "${mod}, mouse:272, movewindow"
          "${mod}, mouse:273, resizewindow"
        ];

        windowrulev2 = [
          "noinitialfocus, workspace:3, floating:1, xwayland:1, class:(), title:(),"
          "noanim, floating:1, class:^(ueberzugpp_)"
          "suppressevent maximize, class:.*"
          "suppressevent fullscreen, class:.*"
        ];
        dwindle = {
          pseudotile = "yes";
          preserve_split = "yes";
          # no_gaps_when_only = "yes";
        };
      };

    extraConfig = ''
      monitor=,preferred,auto,auto
      monitor = eDP-1, 1920x1080@144, 0x0, 1
      monitor = HDMI-A-1, 1920x1080@120, 0x0, 1
      general {
          gaps_in = 6
          gaps_out = 12
          border_size = 3
          col.active_border = rgba(9c63feff) rgba(d4ac77ff) 270deg
          col.inactive_border = rgba(26262600)
          layout = dwindle
      }
      decoration {
          rounding = 9

          drop_shadow = true
          shadow_range = 4
          shadow_render_power = 1
          shadow_ignore_window = true
          col.shadow = rgba(00000055)
          col.shadow_inactive = rgba(00000000)
          shadow_scale = 1
          
          dim_inactive = false
          dim_strength = 0.1
          dim_special = 0.1
          dim_around = 0.1

          blur {
              enabled = true
              size = 32
              passes = 4
              xray = false
              popups = true
          }
      }
      animations {
          enabled = true
          bezier = myBezier, 0.05, 1, 0.1, 1.05
          animation = windows, 1, 3, myBezier
          animation = windowsOut, 1, 3, default, popin 80%
          animation = border, 1, 3, default
          animation = border, 1, 3, default
          animation = fade, 1, 1, default
          animation = workspaces, 1, 1, default
      }
      input {
          kb_layout = us
          kb_variant =
          kb_model =
          kb_options =
          kb_rules =
          numlock_by_default = true
          repeat_rate = 60
          repeat_delay = 200
          follow_mouse = 1
          accel_profile = flat
          sensitivity = 0 # -1.0 - 1.0, 0 means no modification.

          touchpad {
              disable_while_typing = false
              natural_scroll = false
              scroll_factor = 1
              clickfinger_behavior = true
          }
      }
      gestures {
          workspace_swipe = on
          workspace_swipe_invert = false
          workspace_swipe_touch = true
      }
      device { 
        name = syna1202:00-06cb:cd79-touchpad 
        sensitivity = +1.2
      }
      device {
        name = kensington-expert-wireless-tb-mouse
        sensitivity = +2.0
      }
      misc {
          disable_hyprland_logo = true
          vfr = true
          focus_on_activate = false
      }
      dwindle {
          pseudotile = yes
          preserve_split = yes
      }
    '';
  };
}
