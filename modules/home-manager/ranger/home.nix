{ pkgs, ... }:

{
  home = {
    file = {
      "rc.conf" = {
          source = ./dotfiles/rc.conf;
          target = ".config/ranger/rc.conf";
          enable = true;
      };
      "rifle.conf" = {
          source = ./dotfiles/rifle.conf;
          target = ".config/ranger/rifle.conf";
          enable = true;
      };
    };

    packages = with pkgs;
    [
      ranger
    ];
  };
}
