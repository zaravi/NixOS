{ ... }:

{
  home.file."alacritty-nix" = {
    source = ./alacritty-nix.desktop;
    target = ".local/share/applications/alacritty-nix.desktop";
    recursive = false;
    enable = true;
  };
  programs.alacritty = {
    enable = true;
    settings = {
      window = {
        dimensions = {
          lines = 25;
          columns = 80;
        };

        padding = {
          x = 12;
          y = 8;
        };

        decorations = "full";
        # opacity = 0.9;
        blur = true;
        dynamic_title = true;
        decorations_theme_variant = "Dark";
      };

      scrolling = {
        history = 100000;
        multiplier = 1;
      };

      selection.save_to_clipboard = true;
      bell.command = "None";
      mouse = {
        hide_when_typing = true;
      };

      keyboard.bindings = [
        { key = "u"; mods = "Control"; action = "ToggleViMode"; }

        { key = "m"; mode = "Vi"; action = "Left"; }
        { key = "n"; mode = "Vi"; action = "Down"; }
        { key = "e"; mode = "Vi"; action = "Up"; }
        { key = "i"; mode = "Vi"; action = "Right"; }

        { key = "p"; mode = "Vi"; action = "Paste"; }
        { key = "y"; mode = "Vi"; action = "Copy"; }
      ];
    };
  };
}
