{ ... }:

{
  home.file."ghostty-nix" = {
    source = ./ghostty-nix.desktop;
    target = ".local/share/applications/ghostty-nix.desktop";
    recursive = false;
    enable = true;
  };
}
