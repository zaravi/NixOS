{ ... }:

{
  home.file."zed-nix" = {
    source = ./zed-nix.desktop;
    target = ".local/share/applications/zed-nix.desktop";
    recursive = false;
    enable = true;
  };
}
