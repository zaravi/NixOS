{ pkgs, ... }:

{
  programs.helix = {
    enable = true;
    package = pkgs.helix;
    defaultEditor = true;

    ignores = [
      ".build/"
      "!.gitignore"
      "target/"
      "result"
      "zed-cache"
    ];

    settings = {
      theme = "wildhelix";
      editor = {
        line-number = "relative";
        scroll-lines = 1;

        lsp = {
          display-messages = true;
          display-inlay-hints = true;
        };

        cursor-shape = {
          insert = "bar";
          normal = "block";
          select = "underline";
        };

        indent-guides = {
          render = true;
          character = "│";
          skip-levels = 0;
        };
      };

      keys.normal = {
        space = {
          space = "file_picker";
          w = ":w";
          q = ":q";
          f = "file_picker_in_current_directory";
          b = "buffer_picker";
          g = "goto_definition";
          h = "hover";
          r = "rename_symbol";
          a = "code_action";
        };

        esc = [ "collapse_selection" "keep_primary_selection" ];
        # Vim-like
        G = "goto_file_end";
        # Colemak
        u = "insert_mode";
        l = "undo";
        L = "undo";
        m = "move_char_left";
        n = "move_visual_line_down";
        e = "move_visual_line_up";
        i = "move_char_right";
      };

      keys.select = {
        # Colemak
        l = "undo";
        L = "undo";
        m = "move_char_left";
        n = "move_visual_line_down";
        e = "move_visual_line_up";
        i = "move_char_right";
      };
    };

    languages = {
      language-server = {
        # Nix language server
        nil = {
          command = "nil";
          config = {
            nil = {
              formatting = { command = [ "nixpkgs-fmt" ]; };
            };
          };
        };

        # Rust language server
        rust-analyzer = {
          command = "rust-analyzer";
          config = {
            checkOnSave = {
              command = "clippy";
              extraArgs = [ "--all" "--all-features" ];
            };
            inlayHints = {
              bindingModeHints = { enable = true; };
              closureReturnTypeHints = { enable = "always"; };
              discriminantHints = { enable = "always"; };
              lifetimeElisionHints = { enable = "always"; useParameterNames = true; };
              typeHints = { enable = true; hideClosureInitialization = false; hideNamedConstructor = false; };
            };
          };
        };

        # Markdown language server
        marksman = {
          command = "marksman";
          args = [ "server" ];
        };

        # Zig language server
        zls = {
          command = "zls";
        };

        # Optional: Debug adapter for Rust and Zig
        "lldb-dap" = {
          command = "lldb-dap";
        };
      };

      language = [
        # Nix
        {
          name = "nix";
          scope = "source.nix";
          injection-regex = "nix";
          file-types = [ "nix" ];
          # comment-token = "#";
          indent = { tab-width = 2; unit = "  "; };
          language-servers = [ "nil" ];
          formatter = { command = "nixpkgs-fmt"; };
          auto-format = true;
        }

        # Rust
        {
          name = "rust";
          scope = "source.rust";
          injection-regex = "rust";
          file-types = [ "rs" ];
          # comment-token = "//";
          indent = { tab-width = 4; unit = "    "; };
          language-servers = [ "rust-analyzer" ];
          auto-format = true;
          # Optional: Enable debugging
          # debugger = { name = "lldb-dap"; transport = "stdio"; command = "lldb-dap"; };
        }

        # Markdown
        {
          name = "markdown";
          scope = "text.markdown";
          injection-regex = "md|markdown";
          file-types = [ "md" "markdown" ];
          roots = [ ".git" ".hg" ".svn" ];
          # comment-token = "<!--";
          # block-comment-tokens = { start = "<!--"; end = "-->"; };
          language-servers = [ "marksman" ];
          indent = { tab-width = 2; unit = "  "; };
        }

        # Zig
        {
          name = "zig";
          scope = "source.zig";
          injection-regex = "zig";
          file-types = [ "zig" "zir" ];
          # comment-token = "//";
          indent = { tab-width = 4; unit = "    "; };
          language-servers = [ "zls" ];
          auto-format = true;
          formatter = { command = "zig"; args = [ "fmt" "--stdin" ]; };
          # Optional: Enable debugging
          # debugger = { name = "lldb-dap"; transport = "stdio"; command = "lldb-dap"; };
        }
      ];

      grammar = [
        # Nix grammar
        {
          name = "nix";
          source = {
            git = "https://github.com/nix-community/tree-sitter-nix";
            rev = "48057cf966641e7a49b09700550751195c34bcb5";
          };
        }

        # Rust grammar
        {
          name = "rust";
          source = {
            git = "https://github.com/tree-sitter/tree-sitter-rust";
            rev = "bb08b6fe4cb1b5c76f13a0da2bf8957bfe665567";
          };
        }

        # Markdown grammar
        # {
        #   name = "markdown";
        #   source = {
        #     git = "https://github.com/MDeiml/tree-sitter-markdown";
        #     rev = "192407ab5a24bfc24f13332979b5e7967518754a";
        #   };
        # }

        # Zig grammar
        {
          name = "zig";
          source = {
            git = "https://github.com/maxxnino/tree-sitter-zig";
            rev = "a80a6e9be81b33b182ce6305ae4ea28e29211bd5";
          };
        }
      ];
    };

    themes = {
      wildhelix = {
        "ui.menu" = { fg = "#fffafa"; bg = "none"; };
        "ui.menu.selected" = { fg = "#d4ac77"; bg = "none"; modifiers = [ "reversed" ]; };
        "ui.linenr" = { fg = "#666666"; bg = "none"; };
        "ui.popup" = { fg = "#fffafa"; bg = "none"; };
        "ui.linenr.selected" = { fg = "#d4ac77"; bg = "none"; modifiers = [ "bold" ]; };
        "ui.selection" = { fg = "#fffafa"; bg = "#4a4949"; };
        "ui.selection.primary" = { modifiers = [ "reversed" ]; };
        "comment" = { fg = "#666666"; };
        "ui.statusline" = { fg = "#fffafa"; bg = "none"; };
        "ui.statusline.inactive" = { fg = "#666666"; bg = "none"; };
        "ui.help" = { fg = "#d4ac77"; bg = "none"; };
        "ui.cursor" = { modifiers = [ "reversed" ]; };
        "ui.virtual.indent-guide" = { fg = "#666666"; };
        "variable" = "#9c63fe";
        "variable.builtin" = "#d4ac77";
        "constant.numeric" = "#f62a63";
        "constant" = "#f62a63";
        "attributes" = "#57C5ED";
        "type" = "#57C5ED";
        "ui.cursor.match" = { fg = "#d4ac77"; modifiers = [ "underlined" ]; };
        "string" = "#f62a63";
        "variable.other.member" = "#57C5ED";
        "constant.character.escape" = "#9c63fe";
        "function" = "#9c63fe";
        "constructor" = "#57C5ED";
        "special" = "#d4ac77";
        "keyword" = "#9c63fe";
        "label" = "#d4ac77";
        "namespace" = "#57C5ED";
        "diff.plus" = "#57C5ED";
        "diff.delta" = "#d4ac77";
        "diff.minus" = "#f62a63";
        "diagnostic" = { modifiers = [ "underlined" ]; };
        "ui.gutter" = { bg = "none"; };
        "info" = "#57C5ED";
        "hint" = "#666666";
        "debug" = "#666666";
        "warning" = "#d4ac77";
        "error" = "#f62a63";

        # Base syntax elements
        "attribute" = { fg = "#57C5ED"; };
        "comment.line" = { fg = "#666666"; };
        "comment.block" = { fg = "#666666"; };
        "comment.block.documentation" = { fg = "#666666"; modifiers = [ "italic" ]; };
        "constant.builtin" = { fg = "#f62a63"; modifiers = [ "bold" ]; };
        "constant.builtin.boolean" = { fg = "#f62a63"; modifiers = [ "bold" ]; };
        "constant.character" = { fg = "#f62a63"; };
        "constant.numeric.integer" = { fg = "#f62a63"; };
        "constant.numeric.float" = { fg = "#f62a63"; };
        "function.builtin" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "function.method" = { fg = "#9c63fe"; };
        "function.method.private" = { fg = "#9c63fe"; modifiers = [ "italic" ]; };
        "function.macro" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "function.special" = { fg = "#9c63fe"; modifiers = [ "italic" ]; };
        "keyword.control" = { fg = "#9c63fe"; };
        "keyword.control.conditional" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "keyword.control.repeat" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "keyword.control.import" = { fg = "#9c63fe"; };
        "keyword.control.return" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "keyword.control.exception" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "keyword.operator" = { fg = "#9c63fe"; };
        "keyword.directive" = { fg = "#9c63fe"; modifiers = [ "italic" ]; };
        "keyword.function" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "keyword.storage" = { fg = "#9c63fe"; };
        "keyword.storage.type" = { fg = "#9c63fe"; };
        "keyword.storage.modifier" = { fg = "#9c63fe"; };
        "operator" = { fg = "#d4ac77"; };
        "punctuation" = { fg = "#fffafa"; };
        "punctuation.delimiter" = { fg = "#fffafa"; };
        "punctuation.bracket" = { fg = "#fffafa"; };
        "punctuation.special" = { fg = "#d4ac77"; };
        "string.regexp" = { fg = "#f62a63"; modifiers = [ "bold" ]; };
        "string.special" = { fg = "#f62a63"; modifiers = [ "italic" ]; };
        "string.special.path" = { fg = "#f62a63"; modifiers = [ "underlined" ]; };
        "string.special.url" = { fg = "#f62a63"; modifiers = [ "underlined" ]; };
        "string.special.symbol" = { fg = "#f62a63"; modifiers = [ "bold" ]; };
        "tag" = { fg = "#57C5ED"; };
        "tag.builtin" = { fg = "#57C5ED"; modifiers = [ "bold" ]; };
        "type.builtin" = { fg = "#57C5ED"; modifiers = [ "bold" ]; };
        "type.parameter" = { fg = "#57C5ED"; modifiers = [ "italic" ]; };
        "type.enum" = { fg = "#57C5ED"; };
        "type.enum.variant" = { fg = "#d4ac77"; };
        "variable.parameter" = { fg = "#9c63fe"; modifiers = [ "italic" ]; };
        "variable.other.member.private" = { fg = "#57C5ED"; modifiers = [ "italic" ]; };

        # Markup elements (for markdown, etc)
        "markup.heading" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "markup.heading.marker" = { fg = "#d4ac77"; };
        "markup.heading.1" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "markup.heading.2" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "markup.heading.3" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "markup.heading.4" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "markup.heading.5" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "markup.heading.6" = { fg = "#9c63fe"; modifiers = [ "bold" ]; };
        "markup.list" = { fg = "#d4ac77"; };
        "markup.list.numbered" = { fg = "#d4ac77"; };
        "markup.list.unnumbered" = { fg = "#d4ac77"; };
        "markup.list.checked" = { fg = "#57C5ED"; };
        "markup.list.unchecked" = { fg = "#666666"; };
        "markup.bold" = { fg = "#fffafa"; modifiers = [ "bold" ]; };
        "markup.italic" = { fg = "#fffafa"; modifiers = [ "italic" ]; };
        "markup.strikethrough" = { fg = "#666666"; modifiers = [ "crossed_out" ]; };
        "markup.link" = { fg = "#57C5ED"; modifiers = [ "underlined" ]; };
        "markup.link.url" = { fg = "#57C5ED"; modifiers = [ "underlined" ]; };
        "markup.link.label" = { fg = "#9c63fe"; };
        "markup.link.text" = { fg = "#d4ac77"; };
        "markup.quote" = { fg = "#d4ac77"; modifiers = [ "italic" ]; };
        "markup.raw" = { fg = "#f62a63"; };
        "markup.raw.inline" = { fg = "#f62a63"; };
        "markup.raw.block" = { fg = "#f62a63"; };
      };
    };
  };

  # Make sure the necessary packages are available
  home.packages = with pkgs; [
    # Nix
    nil
    nixpkgs-fmt

    # Rust
    # rustup
    rust-analyzer

    # Markdown
    marksman

    # Zig
    zig
    zls

    # Debug support
    lldb
  ];
}
