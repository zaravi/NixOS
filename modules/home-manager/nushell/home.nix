{ pkgs, user, ... }:

{
  home.packages = with pkgs; [
    pfetch
    tree
    btop
  ];

  programs = {
    nushell = {
      enable = true;
      shellAliases = {
        # Elevate for things
        shutdown = "doas shutdown -h now";
        reboot = "doas reboot";
        mount = "doas mount";
        umount = "doas umount";
        cryptsetup = "doas cryptsetup";

        # Ask before running dangerous commands.
        cp = "cp -i";
        mv = "mv -i";
        rm = "rm -i";

        # Shred
        shred = "shred --remove";

        # Navigation
        ".." = "cd ..";
        "..." = "cd ../..";
        "...." = "cd ../../..";
        "....." = "cd ../../../..";
        "......" = "cd ../../../../..";

        # Git.
        lg = "lazygit";
        gcl = "git clone";
        gpl = "git pull";
        gft = "git fetch";
        gad = "git add";
        gcm = "git commit";
        gst = "git checkout";
        gps = "git push";

        iptables = "doas iptables -L -v --line-number -n";

        # Rice shortcuts.
        pf = "pfetch";
        cm = "cmatrix";

        # # ml scripts
        bg = "swww img --fill-color 262626 --transition-type grow --transition-step 1 --transition-duration 1 --transition-fps 144 --resize crop --filter Lanczos3";
      };

      extraConfig =
        let
          linuxPaths =
            if pkgs.stdenv.isLinux then ''
              $env.PATH = ($env.PATH | split row (char esep) | prepend [
                # "/run/current-system/sw/bin"
                # "/home/${user}/nix-profile/bin"
                # "/home/${user}/.local/state/nix/profiles/home-manager/bin"
                # "/nix/var/nix/profiles/default/bin"
                "/home/${user}/.local/bin"
                "/home/${user}/.local/nu"
                "/home/${user}/.cargo/bin"
              ] | uniq | str join (char esep))
            '' else "";
          darwinPaths =
            if pkgs.stdenv.isDarwin then ''
              $env.PATH = ($env.PATH | split row (char esep) | prepend [
                "/run/current-system/sw/bin"
                "/Users/${user}/.nix-profile/bin"
                "/Users/${user}/.local/state/nix/profiles/home-manager/home-path/bin"
                "/Users/${user}/.local/bin"
                "/Users/${user}/.local/nu"
                "/Users/${user}/.cargo/bin"
              ] | uniq | str join (char esep))
            '' else "";
        in
        ''
          $env.config = {
            show_banner: false,
          }
          ${linuxPaths}
          ${darwinPaths}
        '';
    };

    carapace = {
      enable = true;
      enableNushellIntegration = true;
    };

    zoxide = {
      enable = true;
      enableNushellIntegration = true;
    };

    thefuck = {
      enable = true;
      enableNushellIntegration = true;
    };

    atuin = {
      enable = true;
      enableNushellIntegration = true;
    };

    yazi = {
      enable = true;
      enableNushellIntegration = true;
      settings = {
        manager = {
          show_hidden = false;
          sort_by = "modified";
          sort_dir_first = true;
          show_symlink = true;
          wrap = "yes";
          opener = {
            text = [
              { exec = "hx \"$@\""; desc = "Open text files with Helix"; }
            ];
            video = [
              { exec = "celluloid \"$@\""; desc = "Open videos with Celluloid"; }
            ];
            image = [
              { exec = "loupe \"$@\""; desc = "Open images with Loupe"; }
            ];
          };
        };
      };
      keymap = {
        manager = {
          keymap = [
            { on = "m"; run = "leave"; desc = "Go back to parent directory"; }
            { on = "n"; run = "arrow 1"; desc = "Move cursor up"; }
            { on = "e"; run = "arrow -1"; desc = "Move cursor down"; }
            { on = "i"; run = "enter"; desc = "Enter the child directory"; }

            { on = "o"; run = "open"; desc = "Open the selected file"; }
            { on = "O"; run = "open --interactive"; desc = "Open the selected file interactively"; }

            { on = "q"; run = "quit"; desc = "Quit Yazi"; }
          ];
        };
        select = {
          keymap = [
            { on = "n"; run = "arrow 1"; desc = "Move cursor up"; }
            { on = "e"; run = "arrow -1"; desc = "Move cursor down"; }
          ];
        };
        completion = {
          keymap = [
            { on = "n"; run = "arrow 1"; desc = "Move cursor up"; }
            { on = "e"; run = "arrow -1"; desc = "Move cursor down"; }
          ];
        };
      };
    };

    starship = {
      enable = true;
      settings = {
        add_newline = false;
        format = "$status$directory$git_branch$git_status$git_state$package$cmd_duration$character";

        status = {
          format = "[$symbol]($style) ";
          disabled = false;
          symbol = "✖";
          success_symbol = "";
          style = "bold blue";
          map_symbol = true;
        };

        directory = {
          style = "bold green";
          format = "[$path]($style) ";
          truncation_length = 1;
          truncation_symbol = "";
        };

        git_branch = {
          format = "git:($branch) ";
          style = "bold blue";
        };

        git_status = {
          format = "[$ahead_behind$modified]($style) ";
          style = "bold yellow";
          ahead = "↑$\{count\}";
          behind = "↓$\{count\}";
          modified = "✗";
        };

        git_state = {
          format = "\\([$state( $progress_current/$progress_total)]($style)\\) ";
          style = "bright-black";
        };

        package = {
          format = "[$symbol$version]($style) ";
          symbol = " ";
          style = "bold 208";
          display_private = true;
        };

        cmd_duration = {
          min_time = 300000;
          format = "took [$duration]($style) ";
        };

        character = {
          success_symbol = "[❯](fg:blue)[❯](fg:green)[❯](fg:white)";
          error_symbol = "[❯](fg:blue)[❯](fg:green)[❯](fg:white)";
        };
      };
    };
  };
} 
