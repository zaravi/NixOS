{ pkgs, ... }:

{
  stylix = {
    enable = true;
    autoEnable = false;
    targets = {
      gnome.enable = true;
      gtk.enable = true;

      kde.enable = true;

      alacritty.enable = false;
      nushell.enable = true;
      helix.enable = false;
      btop.enable = true;
      lazygit.enable = true;
      fzf.enable = true;
    };

    polarity = "dark";

    base16Scheme = {
      base00 = "262626"; # #262626 Default Background
      base01 = "262626"; # #262626 Lighter Background (Used for status bars, line number and folding marks)
      base02 = "4a4949"; # #262626 Selection Background
      base03 = "666666"; # #333333 Comments, Invisibles, Line Highlighting
      base04 = "999999"; # #333333 Dark Foreground (Used for status bars)
      base05 = "fffafa"; # #fffafa Default Foreground, Caret, Delimiters, Operators
      base06 = "fffafa"; # #fffafa Light Foreground (Not often used)
      base07 = "fffafa"; # #fffafa Light Background (Not often used)

      base08 = "d4ac77"; # #d4ac77 Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
      base09 = "44bfeb"; # #44bfeb Integers, Boolean, Constants, XML Attributes, Markup Link Url
      base0A = "fffafa"; # #fffafa Classes, Markup Bold, Search Text Background
      base0B = "d4ac77"; # #f51252 Strings, Inherited Class, Markup Code, Diff Inserted
      base0C = "fffafa"; # #d4ac77 Support, Regular Expressions, Escape Characters, Markup Quotes
      base0D = "9c63fe"; # #44bfeb Functions, Methods, Attribute IDs, Headings
      base0E = "d4ac77"; # #9c63fe Keywords, Storage, Selector, Markup Italic, Diff Changed
      base0F = "00ff00"; # #00ff00 Deprecated, Opening/Closing Embedded Language Tags, e.g. <?p
    };

    fonts = {
      monospace = {
        package = pkgs.nerd-fonts.mononoki;
        name = "Mononoki Nerd Font";
      };

      sansSerif = {
        package = pkgs.inter;
        name = "Inter";
      };

      serif = {
        package = pkgs.inter;
        name = "Inter";
      };

      sizes = {
        applications = 12;
        terminal = 14;
        desktop = 10;
        popups = 10;
      };
    };

    opacity = {
      applications = 1.0;
      terminal = 0.9;
      desktop = 1.0;
      popups = 1.0;
    };

    cursor = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Ice";
      size = 24;
    };
  };
}
