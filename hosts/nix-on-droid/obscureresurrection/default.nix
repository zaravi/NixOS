{ pkgs
, lib
, home-manager
, ...
}:

lib.nixOnDroidConfiguration {
  pkgs = pkgs;
  home-manager-path = home-manager.outPath;
  modules = [
    ({ ... }: {
      nix = {
        extraOptions = ''
          experimental-features = nix-command flakes
          keep-outputs          = true
        '';
      };

      user = {
        shell = "/data/data/com.termux.nix/files/home/.nix-profile/bin/nu";
      };

      environment = {
        packages = [ pkgs.git ];
        etcBackupExtension = ".bak";
        sessionVariables = {
          EDITOR = "hx";
        };
      };

      home-manager = {
        config = ./home.nix;
        useGlobalPkgs = true;
        extraSpecialArgs = {
          user = "nix-on-droid";
        };
      };

      system.stateVersion = "23.05";
    })
  ];
}
