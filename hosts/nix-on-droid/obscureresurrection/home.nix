{ pkgs, ... }:

{
  imports = [
    ../../../modules/home-manager/nushell/home.nix
    ../../../modules/home-manager/ranger/home.nix
    ../../../modules/home-manager/helix/home.nix
  ];

  home = {
    stateVersion = "23.05";
    packages = with pkgs;
      [
        openssh
        magic-wormhole
      ];
    sessionVariables = {
      NIX_PATH = "nixpkgs=${pkgs.path}";
    };
  };
}
