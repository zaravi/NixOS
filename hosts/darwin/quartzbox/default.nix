{ system
, user
, home-manager
, nix-darwin
, ...
}:

nix-darwin.lib.darwinSystem {
  specialArgs = {
    inherit
      system
      user
      home-manager
      nix-darwin
      ;
  };

  modules = [
    ({ pkgs, ... }: {
      system = {
        activationScripts.postUserActivation.text = ''
          /System/Library/PrivateFrameworks/SystemAdministration.framework/Resources/activateSettings -u
        '';

        defaults = {
          # Clock settings
          menuExtraClock = {
            Show24Hour = true;
            ShowAMPM = false;
            ShowDate = 1;
            ShowDayOfMonth = true;
            ShowDayOfWeek = true;
            ShowSeconds = false;
          };

          # Dock settings
          dock = {
            autohide = false;
            show-recents = false;
            mineffect = "scale";
            minimize-to-application = true;
            launchanim = true;
            static-only = false;
            showhidden = true;
            show-process-indicators = true;
            orientation = "bottom";
            tilesize = 45;

            wvous-tl-corner = 1;
            wvous-tr-corner = 1;
            wvous-bl-corner = 1;
            wvous-br-corner = 1;
          };

          # Finder settings
          finder = {
            _FXShowPosixPathInTitle = true;
            AppleShowAllExtensions = true;
            AppleShowAllFiles = true;
            FXEnableExtensionChangeWarning = false;
            QuitMenuItem = true;
            ShowPathbar = true;
            ShowStatusBar = true;
            FXPreferredViewStyle = "clmv";
            FXDefaultSearchScope = "SCcf";
          };

          # Trackpad settings
          trackpad = {
            Clicking = true;
            TrackpadRightClick = true;
            TrackpadThreeFingerDrag = false;
            ActuationStrength = 1;
            FirstClickThreshold = 1;
            SecondClickThreshold = 1;
          };

          # Global domain settings
          NSGlobalDomain = {
            "com.apple.swipescrolldirection" = false;
            "com.apple.sound.beep.feedback" = 0;
            AppleInterfaceStyle = "Dark";
            AppleKeyboardUIMode = 3;
            ApplePressAndHoldEnabled = true;
            InitialKeyRepeat = 13;
            KeyRepeat = 4;
            NSAutomaticCapitalizationEnabled = false;
            NSAutomaticDashSubstitutionEnabled = false;
            NSAutomaticPeriodSubstitutionEnabled = false;
            NSAutomaticQuoteSubstitutionEnabled = false;
            NSAutomaticSpellingCorrectionEnabled = false;
            NSNavPanelExpandedStateForSaveMode = true;
            NSNavPanelExpandedStateForSaveMode2 = true;
            AppleShowAllExtensions = true;
            AppleShowScrollBars = "Always";
            NSTableViewDefaultSizeMode = 2;
            AppleTemperatureUnit = "Celsius";
            AppleMeasurementUnits = "Centimeters";
          };

          # LaunchServices settings
          LaunchServices = {
            LSQuarantine = false;
          };

          # Firewall settings
          alf = {
            globalstate = 1;
            allowsignedenabled = 1;
            allowdownloadsignedenabled = 1;
            stealthenabled = 1;
          };

          # Login window settings
          loginwindow = {
            GuestEnabled = false;
            SHOWFULLNAME = true;
            DisableConsoleAccess = true;
          };

          # Spaces settings
          spaces.spans-displays = false;

          # Screenshots settings
          screencapture = {
            location = "~/Pictures/Screenshots";
            type = "png";
            disable-shadow = true;
          };

          # Custom user preferences
          CustomUserPreferences = {
            ".GlobalPreferences" = {
              AppleSpacesSwitchOnActivate = true;
            };
            NSGlobalDomain = {
              WebKitDeveloperExtras = true;
            };
            "com.apple.finder" = {
              ShowExternalHardDrivesOnDesktop = false;
              ShowHardDrivesOnDesktop = false;
              ShowMountedServersOnDesktop = false;
              ShowRemovableMediaOnDesktop = false;
              _FXSortFoldersFirst = true;
              FXDefaultSearchScope = "SCcf";
            };
            # Disable Spotlight Suggestions
            "com.apple.spotlight" = {
              WebSearchEnabled = false;
              VolumeImagesEnabled = false;
              SuggestionsEnabled = false;
            };
            # Disable Location Services
            "com.apple.locationd" = {
              LocationServicesEnabled = false;
            };

            # Disable automatic submission of diagnostic reports
            "com.apple.CrashReporter" = {
              AutoSubmit = false;
              AutoSubmitVersion = 4;
            };

            # Disable Handoff
            "com.apple.coreservices.useractivityd" = {
              ActivityAdvertisingAllowed = false;
              ActivityReceivingAllowed = false;
            };

            # Disable Siri and dictation
            "com.apple.assistant.support" = {
              AssistantEnabled = false;
            };
            "com.apple.speech.recognition.AppleSpeechRecognition.prefs" = {
              DictationIMMasterDictationEnabled = false;
            };

            # Disable Captive Portal
            "com.apple.captive.control" = {
              Active = false;
            };
            "com.apple.desktopservices" = {
              DSDontWriteNetworkStores = true;
              DSDontWriteUSBStores = true;
            };
            "com.apple.screensaver" = {
              askForPassword = 0;
            };
            "com.apple.screencapture" = {
              location = "~/Pictures/Screenshots/macos";
              type = "png";
            };
            "com.apple.AdLib" = {
              allowApplePersonalizedAdvertising = false;
              privacyViewedVersion = 3;
            };
            "com.apple.SoftwareUpdate" = {
              AutomaticCheckEnabled = false;
              AutomaticDownload = 0;
              CriticalUpdateInstall = 0;
              AutomaticallyInstallMacOSUpdates = false;
            };
            "com.apple.TimeMachine".DoNotOfferNewDisksForBackup = true;
            "com.apple.ImageCapture".disableHotPlug = true;
            "com.apple.commerce".AutoUpdate = false;
          };
        };
      };

      networking = {
        knownNetworkServices = [ "Wi-Fi" "Ethernet" "Thunderbolt Bridge" ];
        dns = [ "9.9.9.9" ];
      };

      users.users.zaravi = {
        name = "zaravi";
        home = "/Users/zaravi";
        shell = pkgs.nushell;
      };

      homebrew = {
        enable = true;
        onActivation = {
          autoUpdate = true;
          cleanup = "zap";
          upgrade = true;
        };
        global = {
          brewfile = true;
          lockfiles = true;
        };
        brews = [
          "magic-wormhole"
          "zoxide"
          "atuin"
          "zig"
        ];
        casks = [
          "ghostty"
          "alacritty"
          "bitwig-studio"
          "zen-browser"
          "brave-browser"
          "zed"
          "ilok-license-manager"
          "izotope-product-portal"
          "lulu"
          "native-access"
          "nextcloud"
          "obsidian"
          "syncthing"
          "obs"
          "vlc"
          "linearmouse"
        ];
      };

      services.gitlab-runner.enable = true;

      nix = {
        enable = true;
        package = pkgs.nix;
        gc = {
          automatic = true;
          interval = { Hour = 3; Minute = 15; };
          options = "--delete-older-than 30d";
        };
        optimise.automatic = true;
        settings = {
          experimental-features = [ "nix-command" "flakes" ];
          keep-outputs = true;
          keep-derivations = true;
        };
      };

      security.pam.services.sudo_local.touchIdAuth = false;
      system.keyboard = {
        enableKeyMapping = true;
        remapCapsLockToEscape = true;
      };
      system.stateVersion = 4;
      nixpkgs.hostPlatform = "aarch64-darwin";
    })

    home-manager.darwinModules.home-manager
    {
      home-manager = {
        useGlobalPkgs = true;
        useUserPackages = true;
        extraSpecialArgs = {
          inherit user;
        };
        users."${user}" = {
          imports =
            [ (import ./home.nix) ] ++ # "Core."
            [ (import ../../../modules/home-manager/ranger/home.nix) ] ++
            [ (import ../../../modules/home-manager/nushell/home.nix) ] ++
            [ (import ../../../modules/home-manager/helix/home.nix) ];
        };
      };
    }
  ];
}
