{ user, lib, ... }:

{
  programs = {
    home-manager.enable = true;
  };

  home = {
    stateVersion = "23.05";
    username = "${user}";
    homeDirectory = lib.mkForce "/Users/${user}";
  };
}
