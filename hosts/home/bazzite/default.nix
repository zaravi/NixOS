{ pkgs
, user
, inputs
, home-manager
, stylix
, nix-flatpak
, ...
}:

home-manager.lib.homeManagerConfiguration {
  inherit pkgs;
  extraSpecialArgs = {
    inherit
      pkgs
      user
      inputs;
  };

  modules = [
    ./home.nix
    stylix.homeManagerModules.stylix
    ../../../modules/home-manager/stylix/home.nix
    ../../../modules/home-manager/nushell/home.nix
    ../../../modules/home-manager/ranger/home.nix
    # ../../../modules/home-manager/helix/home.nix
    # ../../../modules/home-manager/alacritty/home.nix
  ];
}
