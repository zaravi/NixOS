{ pkgs, user, ... }:

{
  nix = {
    package = pkgs.nix;
    settings.experimental-features = [ "nix-command" "flakes" ];
  };
  nixpkgs.config.allowUnfree = true;
  programs.home-manager.enable = true;
  services.syncthing.enable = true;

  home = {
    username = "${user}";
    homeDirectory = "/home/${user}";

    stateVersion = "24.05";

    packages = with pkgs; [

      # Graphical Essentials
      nixgl.nixGLIntel
      nixgl.nixVulkanIntel

      # chat
      signal-desktop

      # screencapture
      obs-studio

      # Remote desktop
      remmina

      # Text
      obsidian
      gamemode

      # Nix
      nil
      nixpkgs-fmt

      # Rust
      # rustup
      rust-analyzer

      # Markdown
      marksman

      # Zig
      zig
      zls

      # Debug support
      lldb
    ];
  };
}
