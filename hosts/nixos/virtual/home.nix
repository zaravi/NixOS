{ pkgs, user, ... }:

{
  programs = {
    home-manager.enable = true;
  };

  home = {
    stateVersion = "23.05";
    username = "${user}";
    homeDirectory = "/home/${user}";
    packages = with pkgs;
    [
      firefox
    ];
  };
}
