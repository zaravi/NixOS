{ lib
, inputs
, system
, home-manager
, user
, nix-flatpak
, ...
}:

lib.nixosSystem {
  inherit system;
  specialArgs = { inherit user inputs; };
  modules = [
    /etc/nixos/hardware-configuration.nix
    (

      { pkgs, user, ... }:

      {
        system.stateVersion = "23.05";
        documentation.nixos.enable = false;
        services.qemuGuest.enable = true;

        # Boot needs to be here because it doesn't get generated in hwconfig.
        boot.loader = {
          efi.canTouchEfiVariables = true;
          systemd-boot = {
            enable = true;
            configurationLimit = 10;
          };
        };

        # Single user setup.
        users = {
          mutableUsers = true;
          users.${user} =
            {
              isNormalUser = true;
              extraGroups =
                [
                  "wheel"
                  "video"
                  "audio"
                  "camera"
                  "networkmanager"
                ];
            };
        };

        networking = {
          useDHCP = false;
          networkmanager.enable = true;
        };

        environment = {
          systemPackages = [ pkgs.git ];
          binsh = "${pkgs.dash}/bin/dash";
        };

      }
    )

    # Desktop
    nix-flatpak.nixosModules.nix-flatpak

    home-manager.nixosModules.home-manager
    {
      home-manager = {
        useGlobalPkgs = true;
        useUserPackages = true;
        backupFileExtension = "bak";
        extraSpecialArgs = {
          inherit user inputs;
        };
        users.${user} = {
          imports =
            [ (import ./home.nix) ] ++
            [ (import ../../../modules/nixos/home.nix) ] ++
            [ (import ../../../modules/home-manager/helix/home.nix) ];
        };
      };
    }
  ];
}
